﻿namespace Billennium.OLX.BusinessLogic.DTOs
{
    public class UserAuthenticationDto
    {
        public int Id { get; set;}
        public string Login { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
