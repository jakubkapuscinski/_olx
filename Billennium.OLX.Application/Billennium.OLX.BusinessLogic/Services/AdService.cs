﻿using AutoMapper;
using Billennium.OLX.BusinessLogic.DTOs;
using Billennium.OLX.BusinessLogic.Helpers;
using Billennium.OLX.BusinessLogic.Interfaces;
using Billennium.OLX.ContextData.Entities;
using Billennium.OLX.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Billennium.OLX.BusinessLogic.Services
{
    public class AdService : IAdService
    {
        private readonly IAdRepository _adRepository;
        private  readonly IMapper _mapper;
        public AdService(IAdRepository adRepository, IMapper mapper)
        {
            _adRepository = adRepository;
            _mapper = mapper;
        }

        public async Task<AdDTO> Add(AdDTO ad)
        {
            var mappedAd = _mapper.Map<Ad>(ad);
            mappedAd.CreationDate = DateTime.Now;
            mappedAd.ExpirationDate = DateTime.Now.AddDays(14);

            var dbAd = await _adRepository.Add(mappedAd);

            return _mapper.Map<AdDTO>(dbAd); 
        }

        public async Task<AdDTO> GetById(int id)
        {
            var ad = await _adRepository.GetById(id);
            if(ad == null)
            {
                throw new Exception(BusinessLogicErrorMessages.AdNotFound);
            }

            var adDTO = _mapper.Map<AdDTO>(ad);

            return adDTO;
        }

        public async Task<IEnumerable<AdDTO>> GetAll()
        {
            IEnumerable<Ad> ads = await _adRepository.GetAll();
            IEnumerable<AdDTO> adsDTO = _mapper.Map<IEnumerable<AdDTO>>(ads);

            return  adsDTO;
        }

        public async Task Delete(int id)
        {
            var ad = _adRepository.GetById(id);
            if (ad == null)
            {
                throw new Exception(BusinessLogicErrorMessages.AdNotFound);
            }

            await _adRepository.Delete(id);
        }

        public async  Task<AdDTO> Update(AdUpdateDTO adDTO)
        {
            var ad = await _adRepository.GetById(adDTO.Id);
            if (ad == null)
            {
                throw new Exception(BusinessLogicErrorMessages.AdNotFound);
            }

            _mapper.Map(adDTO, ad);

            var updatedAd = await _adRepository.Update(ad);

            return _mapper.Map<AdDTO>(updatedAd);
        }

    }
}
