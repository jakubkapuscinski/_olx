﻿using System;
using System.Collections.Generic;

namespace Billennium.OLX.BusinessLogic.DTOs
{
    public class AdDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public bool IsPriceNegotiable { get; set; }
        public DateTime CreationDate { get; set; } 
        public DateTime ExpirationDate { get; set; } 
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public ICollection<PictureDTO> Pictures { get; set; }
    }
}
