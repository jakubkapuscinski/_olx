﻿using System.ComponentModel.DataAnnotations;
namespace Billennium.OLX.ContextData.Entities
{
    public class TagValue
    {
        [Key]
        public int Id { get; set; }
        public string Value { get; set; }
        public Ad Ad { get; set; }
        public Tag Tag { get; set; }

    }
}
