﻿using Billennium.OLX.ContextData;
using Billennium.OLX.ContextData.Entities;
using Billennium.OLX.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Billennium.OLX.Repository.Repositories
{
    public class AdRepository: IAdRepository
    {
        private OLXContext _OLXdbContext;
        public AdRepository(OLXContext OLXdbContext)
        {
            _OLXdbContext = OLXdbContext;
        }

        public async Task<Ad> Add(Ad ad)
        {
            await _OLXdbContext.Ads.AddAsync(ad);
            await _OLXdbContext.SaveChangesAsync();
            return ad;
        }

        public async Task<IEnumerable<Ad>> GetAll()
        {
            return await _OLXdbContext.Ads.Include(c => c.Category)
                 .Include(u => u.User)
                 .Include(p => p.Pictures)
                 .OrderByDescending(x => x.CreationDate)
                 .ToListAsync();
        }

        public async Task<Ad> GetById(int id)
        {
            return await _OLXdbContext.Ads.Include(c => c.Category)
                 .Include(u => u.User)
                 .Include(p => p.Pictures)
                 .FirstAsync(p => p.Id == id);
        }

        public async Task Delete(int id)
        {
             var ad = await _OLXdbContext.Ads.FindAsync(id);
            _OLXdbContext.Ads.Remove(ad);
            _OLXdbContext.SaveChanges();
        }

        public async Task<Ad> Update(Ad ad)
        {
            _OLXdbContext.Ads.Update(ad);
            await _OLXdbContext.SaveChangesAsync();

            return ad;
        }
    }
}
