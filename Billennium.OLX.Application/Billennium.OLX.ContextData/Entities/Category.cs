﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Billennium.OLX.ContextData.Entities
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string CategoryName { get; set; }
        public  Category Parent { get; set; }
        public ICollection<Category> Children { get; set; }
        public Tag Tag {get; set; }
    }
}
