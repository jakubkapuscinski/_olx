﻿using System.ComponentModel.DataAnnotations;

namespace Billennium.OLX.ContextData.Entities
{
    public class Picture
    {
        [Key]
        public int Id { get; set; }
        public string Image { get; set; }
        public bool IsPictureMiniature  { get; set; }
        public int OrderNumber { get; set; }
        public Ad Ad { get; set; }
    }
}
