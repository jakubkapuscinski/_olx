﻿namespace Billennium.OLX.Repository.Helpers
{
    public static class RepositoryErrorMessages
    {
        public const string WrongPasswordFormat  = "Value cannot be empty or whitespace only string.";
        public const string HashInvalidLength  = "Invalid length of password hash (64 bytes expected).";
        public const string SaltInvalidLenght  = "Invalid length of password salt (128 bytes expected).";
    }
}
