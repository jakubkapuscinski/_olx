﻿using System;
using System.Threading.Tasks;
using Billennium.OLX.BusinessLogic.DTOs;
using Billennium.OLX.BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Billennium.OLX.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AdsController : ControllerBase
    {
        private readonly IAdService _adService;

        public AdsController(IAdService adService)
        {
            _adService = adService;
        }

        [HttpPost]
        public async Task<ActionResult> NewAd([FromBody] AdDTO adDTO)
        {
            try
            {
                return Ok(await _adService.Add(adDTO));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<ActionResult> GetAds()
        {
            try
            {
                return Ok(await _adService.GetAll());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            try
            {
                return Ok(await _adService.GetById(id));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _adService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> UpdateAdData(AdUpdateDTO ad)
        {
            try
            {
                return Ok(await _adService.Update(ad));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}