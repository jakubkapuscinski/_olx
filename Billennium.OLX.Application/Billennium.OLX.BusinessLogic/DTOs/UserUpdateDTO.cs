﻿namespace Billennium.OLX.BusinessLogic.DTOs
{
    public class UserUpdateDTO 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsPhoneNumberVisible { get; set; }
        public string Email { get; set; }
        public bool IsEmailVisible { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HomeNumber { get; set; }
    }
}
