﻿using AutoMapper;
using Billennium.OLX.BusinessLogic.DTOs;
using Billennium.OLX.ContextData.Entities;
namespace Billennium.OLX.BusinessLogic.Automapper
{
    public class AutoMapperProfile:Profile
    {
        public  AutoMapperProfile()
        {
            CreateMap<User, UserDto>()
                .ForPath(dest => dest.Address, opts => opts.MapFrom(src => new AddressDTO
                {
                    Street = src.Street,
                    City = src.City,
                    HomeNumber = src.HomeNumber,
                    PostalCode = src.PostalCode
                }))
                .ReverseMap();
            CreateMap<User,UserAuthenticationDto>().ReverseMap();
            CreateMap<User, RegisterDTO>().ReverseMap();
            CreateMap<User, UserUpdateDTO>().ReverseMap();
            CreateMap<Picture, PictureDTO>().ReverseMap();
            CreateMap<Category, CategoryDTO>().ReverseMap();
            CreateMap<Ad, AdDTO>().ReverseMap();
            CreateMap<Ad, AdUpdateDTO>().ReverseMap();
        }
    }
}
//source, destination