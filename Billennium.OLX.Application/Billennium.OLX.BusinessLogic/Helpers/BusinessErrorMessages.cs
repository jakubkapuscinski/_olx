﻿namespace Billennium.OLX.BusinessLogic.Helpers
{
    public static class BusinessLogicErrorMessages
    {
        public const string LoginPasswordNotProvided  = "You need to provide login and password";
        public const string LoginPasswordIncorrect  = "Login or password is incorrect";
        public const string PasswordRequired  = "Password is required";
        public const string UserExists = "User already exists";
        public const string AdNotFound = "Ad does not exist";
        public const string UserNotFound  = "User not found";
    }
}
