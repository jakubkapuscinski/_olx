﻿using Microsoft.AspNetCore.Builder;

namespace Billennium.OLX.Api.Extensions
{
    public static class ConfgureBuilderExtensions
    {
        public static void AddCors(this IApplicationBuilder app)
        {
            app.UseCors(options =>
            {
                options.AllowAnyOrigin();
                options.AllowAnyMethod();
                options.AllowAnyHeader();
            });
        }

        public static void AddSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Default");
            });
        }
    }
}

