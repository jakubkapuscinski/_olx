﻿using Billennium.OLX.ContextData.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Billennium.OLX.Repository.Interfaces
{
    public interface IUserRepository
    {
        Task<User> GetByLogin(string login);
        Task<User> Add(User user);
        Task<IEnumerable<User>> GetAll();
        Task<User> Update(User user);
        Task<User> GetById(int id);
        Task Delete(int id);
        Task<bool> CheckIfUserExists(User user);
    }
}
