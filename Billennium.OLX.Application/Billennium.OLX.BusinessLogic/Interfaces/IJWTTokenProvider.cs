﻿using Billennium.OLX.ContextData.Entities;

namespace Billennium.OLX.BusinessLogic.Interfaces
{
    public interface IJWTTokenProvider
    {
        string GenerateToken(User user);
    }
}

