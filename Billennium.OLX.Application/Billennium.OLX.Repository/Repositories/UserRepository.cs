﻿using Billennium.OLX.ContextData;
using Billennium.OLX.ContextData.Entities;
using Billennium.OLX.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Billennium.OLX.Repository.Repositories
{
    public class UserRepository : IUserRepository
    {
        private OLXContext _OLXdbContext;
        public UserRepository(OLXContext OLXdbContext)
        {
            _OLXdbContext = OLXdbContext;
        }

        public async Task<User> Add(User user)
        {
            await _OLXdbContext.Users.AddAsync(user);
            await _OLXdbContext.SaveChangesAsync();
            return user;
        }

        public async Task<bool> CheckIfUserExists(User user)
        {
            return await _OLXdbContext.Users
                .AsNoTracking().
                AnyAsync(x => x.Login == user.Login);
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _OLXdbContext.Users
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<User> Update(User user)
        {
            _OLXdbContext.Users.Update(user);
            await _OLXdbContext.SaveChangesAsync();
            return user;
        }

        public async Task<User> GetByLogin(string login)
        {
            var user = await _OLXdbContext.Users
                .AsNoTracking()
                .FirstAsync(x => x.Login == login);

            return user;
        }

        public async Task<User> GetById(int id)
        {
            var user = await _OLXdbContext.Users
                .AsNoTracking()
                .Include(p => p.Ads).
                SingleOrDefaultAsync(t => t.Id == id);

            return user;
        }

        public async Task Delete(int id)
        {
            var user = await _OLXdbContext.Users.FindAsync(id);
            _OLXdbContext.Users.Remove(user);
            _OLXdbContext.SaveChanges();
        }
    }
}
