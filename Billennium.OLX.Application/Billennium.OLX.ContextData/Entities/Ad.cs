﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Billennium.OLX.ContextData.Entities
{
    public class Ad
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public bool IsPriceNegotiable { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public ICollection<Favourites> Favourites { get; set; }
        public ICollection<Picture> Pictures { get; set; }
        public ICollection<TagValue> TagValues { get; set; }
    }
}
