﻿using Billennium.OLX.ContextData.Entities;
using Microsoft.EntityFrameworkCore;

namespace Billennium.OLX.ContextData
{
    public class OLXContext:DbContext
    {
        public OLXContext(DbContextOptions<OLXContext> options): base(options)
        {

        }
        public DbSet<Ad> Ads { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Favourites> Favourites { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagValue> TagValues { get; set; }
        public DbSet<User> Users { get; set; }

    } 
}
