﻿namespace Billennium.OLX.BusinessLogic.DTOs
{
    public class PictureDTO
    {
        public string Image { get; set; }
        public bool IsPictureMiniature { get; set; }
        public int OrderNumber { get; set; }
    }
}
