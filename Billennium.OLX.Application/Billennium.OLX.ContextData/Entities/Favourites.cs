﻿using System.ComponentModel.DataAnnotations;

namespace Billennium.OLX.ContextData.Entities
{
    public class Favourites
    {
        [Key]
        public int Id { get; set; }
        public User User { get; set; }
        public Ad Ad { get; set; }
    }
}
