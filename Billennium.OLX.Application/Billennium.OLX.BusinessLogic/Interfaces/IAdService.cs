﻿using Billennium.OLX.BusinessLogic.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Billennium.OLX.BusinessLogic.Interfaces
{
    public interface IAdService
    {
        Task<AdDTO> GetById(int id);
        Task<IEnumerable<AdDTO>> GetAll();
        Task<AdDTO> Add(AdDTO ad);
        Task Delete(int id);
        Task<AdDTO> Update(AdUpdateDTO ad);
    }
}
