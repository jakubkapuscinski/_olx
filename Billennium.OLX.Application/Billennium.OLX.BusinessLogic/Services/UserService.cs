﻿using AutoMapper;
using Billennium.OLX.BusinessLogic.DTOs;
using Billennium.OLX.BusinessLogic.Helpers;
using Billennium.OLX.BusinessLogic.Interfaces;
using Billennium.OLX.ContextData.Entities;
using Billennium.OLX.Repository.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Billennium.OLX.BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IJWTTokenProvider _jwtToken;
      
        public UserService(IUserRepository userRepository, IMapper mapper, IPasswordHasher<User> passwordHasher, IJWTTokenProvider jwtToken)
        {
            _jwtToken = jwtToken;
            _passwordHasher = passwordHasher;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserDto>> GetAll()
        {
            var users = await _userRepository.GetAll();
            var usersDto = _mapper.Map<List<UserDto>>(users);

            return usersDto;
        }

        public async Task<RegisterDTO> Add(RegisterDTO registerDTO)
        {
            var user = _mapper.Map<User>(registerDTO);

            if (string.IsNullOrWhiteSpace(registerDTO.Password))
            {
                throw new Exception(BusinessLogicErrorMessages.PasswordRequired);
            }

            if (await _userRepository.CheckIfUserExists(user))
            {
                throw new Exception(BusinessLogicErrorMessages.UserExists);
            }

            var userPasswordHash = AddPasswordHash(user,registerDTO);
            var newUser = await _userRepository.Add(userPasswordHash);

            return _mapper.Map<RegisterDTO>(newUser);
        }

        public async Task<UserUpdateDTO> Update(UserUpdateDTO userDto)
        {
            var user = await _userRepository.GetById(userDto.Id);

            if (user == null)
            {
                throw new Exception(BusinessLogicErrorMessages.UserNotFound);
            }

            _mapper.Map(userDto,user);
            var editedUser = await _userRepository.Update(user);
            var mappedEditedUser = _mapper.Map<UserUpdateDTO>(editedUser);

            return mappedEditedUser;
        }

        public async Task<UserAuthenticationDto> Authenticate(UserAuthenticationDto userAuthDTO)
        {
            if (string.IsNullOrEmpty(userAuthDTO.Login) || string.IsNullOrEmpty(userAuthDTO.Password))
            {
                throw new Exception(BusinessLogicErrorMessages.LoginPasswordNotProvided);
            }
           
            var user = await _userRepository.GetByLogin(userAuthDTO.Login);
            _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, userAuthDTO.Password);

            string tokenString = _jwtToken.GenerateToken(user);
            var userWithToken = _mapper.Map<UserAuthenticationDto>(user);
            userWithToken.Token = tokenString;

            return userWithToken;
        }

        public async Task<UserDto> GetById(int id)
        {
            var user = await _userRepository.GetById(id);

            if (user == null)
            {
                throw new Exception(BusinessLogicErrorMessages.UserNotFound);
            }

            var userDto = _mapper.Map<UserDto>(user);
            return userDto;
        }

        public async Task Delete(int id)
        {
            var user = _userRepository.GetById(id);

            if (user == null)
            {
                throw new Exception(BusinessLogicErrorMessages.UserNotFound);
            }

            await _userRepository.Delete(id);
        }
        private User AddPasswordHash(User user,RegisterDTO registerDTO)
        {
          var passwordHash = _passwordHasher.HashPassword(user, registerDTO.Password);
          user.AddPasswordHash(passwordHash);
          
          return user;
        }
    }
}
