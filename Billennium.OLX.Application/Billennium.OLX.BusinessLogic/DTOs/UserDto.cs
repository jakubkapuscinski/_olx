﻿using System.Collections.Generic;

namespace Billennium.OLX.BusinessLogic.DTOs
{
    public class UserDto
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsPhoneNumberVisible { get; set; }
        public string Email { get; set; }
        public bool IsEmailVisible { get; set; }
        public ICollection<AdDTO> Ads { get; set; }
        public AddressDTO Address { get; set; }
    }
}
