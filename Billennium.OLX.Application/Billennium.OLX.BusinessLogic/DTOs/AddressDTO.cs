﻿namespace Billennium.OLX.BusinessLogic.DTOs
{
    public class AddressDTO
    {
        public string City { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string HomeNumber { get; set; }
    }
}
