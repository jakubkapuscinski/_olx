﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Billennium.OLX.ContextData.Entities
{
    public class Tag
    {
        [Key]
        public int Id { get; set; }
        public string TagName { get; set; }
        public ICollection<Category> Categories { get; set;}
        public ICollection<TagValue> TagValues { get; set; }
    }
}
