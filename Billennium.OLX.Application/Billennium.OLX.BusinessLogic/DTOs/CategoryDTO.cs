﻿namespace Billennium.OLX.BusinessLogic.DTOs
{
    public class CategoryDTO
    {
        public string CategoryName { get; set; }
    }
}
