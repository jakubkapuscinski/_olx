﻿using AutoMapper;
using Billennium.OLX.BusinessLogic.Automapper;
using Billennium.OLX.BusinessLogic.Services;
using Billennium.OLX.Repository.Interfaces;
using Billennium.OLX.Repository.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Text;
using Billennium.OLX.BusinessLogic.Helpers;
using Billennium.OLX.BusinessLogic.Interfaces;
using Billennium.OLX.ContextData.Entities;
using Microsoft.AspNetCore.Identity;

namespace Billennium.OLX.Api.Extensions
{
    public static class ServiceCollectionsExtensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IAdRepository,AdRepository>();
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IUserService,UserService>();
            services.AddScoped<IAdService,AdService>();
            services.AddScoped<IPasswordHasher<User>,PasswordHasher<User>>();
            services.AddScoped<IJWTTokenProvider, JWTTokenProviderService>();
        }

        public static void AddJWT(this IServiceCollection services, IConfiguration configuration)
        {
            
            var appSettingsSection = configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
         
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters 
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };

            });
        }

        public static void AddAutoMapper(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>();
            });
            var mapper = configuration.CreateMapper();
            services.AddSingleton(configuration);
            services.AddSingleton(mapper);
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info { Title = "Billennium.OLX.Api", Version = "V1" });
                var xmlFile = $"{typeof(Startup).Assembly.GetName().Name}.XML";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }
    }
}
