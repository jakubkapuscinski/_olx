﻿using System;
using System.Threading.Tasks;
using Billennium.OLX.BusinessLogic.DTOs;
using Billennium.OLX.BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Billennium.OLX.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IUserService _userService;

        public AuthenticationController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("register")]
        public async Task<ActionResult> Register([FromBody] RegisterDTO userDTO)
        {
            try
            {
                return Ok(await _userService.Add(userDTO));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("login")]
        public async Task<ActionResult> Login([FromBody] UserAuthenticationDto userAuthDto)
        {
            try
            {
                return Ok(await _userService.Authenticate(userAuthDto));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}