﻿using Billennium.OLX.ContextData.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Billennium.OLX.Repository.Interfaces
{
    public interface IAdRepository
    {
        Task<Ad> Add(Ad ad);
        Task<IEnumerable<Ad>> GetAll();
        Task<Ad> Update(Ad ad);
        Task<Ad> GetById(int id);
        Task Delete(int id);
    }
}
