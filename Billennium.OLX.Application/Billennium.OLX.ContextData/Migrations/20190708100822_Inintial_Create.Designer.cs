﻿// <auto-generated />
using System;
using Billennium.OLX.ContextData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Billennium.OLX.ContextData.Migrations
{
    [DbContext(typeof(OLXContext))]
    [Migration("20190708100822_Inintial_Create")]
    partial class Inintial_Create
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.Ad", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("CategoryId");

                    b.Property<DateTime>("CreationDate");

                    b.Property<string>("Description");

                    b.Property<DateTime>("ExpirationDate");

                    b.Property<bool>("IsPriceNegotiable");

                    b.Property<string>("Name");

                    b.Property<double>("Price");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("UserId");

                    b.ToTable("Ads");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CategoryName");

                    b.Property<int?>("ParentId");

                    b.Property<int?>("TagId");

                    b.HasKey("Id");

                    b.HasIndex("ParentId");

                    b.HasIndex("TagId");

                    b.ToTable("Categories");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.Favourites", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AdId");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("AdId");

                    b.HasIndex("UserId");

                    b.ToTable("Favourites");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.Picture", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AdId");

                    b.Property<byte[]>("Image");

                    b.Property<bool>("IsPictureMiniature");

                    b.Property<int>("OrderNumber");

                    b.HasKey("Id");

                    b.HasIndex("AdId");

                    b.ToTable("Pictures");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.Tag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("TagName");

                    b.HasKey("Id");

                    b.ToTable("Tags");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.TagValue", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AdId");

                    b.Property<int?>("TagId");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.HasIndex("AdId");

                    b.HasIndex("TagId");

                    b.ToTable("TagValues");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("City");

                    b.Property<string>("Email");

                    b.Property<string>("HomeNumber");

                    b.Property<bool>("IsEmailVisible");

                    b.Property<bool>("IsPhoneNumberVisible");

                    b.Property<string>("LastName");

                    b.Property<string>("Login");

                    b.Property<string>("Name");

                    b.Property<string>("Password");

                    b.Property<string>("PhoneNumber");

                    b.Property<string>("PostalCode");

                    b.Property<string>("Street");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.Ad", b =>
                {
                    b.HasOne("Billennium.OLX.ContextData.Entities.Category", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryId");

                    b.HasOne("Billennium.OLX.ContextData.Entities.User", "User")
                        .WithMany("Ads")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.Category", b =>
                {
                    b.HasOne("Billennium.OLX.ContextData.Entities.Category", "Parent")
                        .WithMany("Children")
                        .HasForeignKey("ParentId");

                    b.HasOne("Billennium.OLX.ContextData.Entities.Tag", "Tag")
                        .WithMany("Categories")
                        .HasForeignKey("TagId");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.Favourites", b =>
                {
                    b.HasOne("Billennium.OLX.ContextData.Entities.Ad", "Ad")
                        .WithMany()
                        .HasForeignKey("AdId");

                    b.HasOne("Billennium.OLX.ContextData.Entities.User", "User")
                        .WithMany("Favourites")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.Picture", b =>
                {
                    b.HasOne("Billennium.OLX.ContextData.Entities.Ad", "Ad")
                        .WithMany()
                        .HasForeignKey("AdId");
                });

            modelBuilder.Entity("Billennium.OLX.ContextData.Entities.TagValue", b =>
                {
                    b.HasOne("Billennium.OLX.ContextData.Entities.Ad", "Ad")
                        .WithMany()
                        .HasForeignKey("AdId");

                    b.HasOne("Billennium.OLX.ContextData.Entities.Tag", "Tag")
                        .WithMany("TagValues")
                        .HasForeignKey("TagId");
                });
#pragma warning restore 612, 618
        }
    }
}
