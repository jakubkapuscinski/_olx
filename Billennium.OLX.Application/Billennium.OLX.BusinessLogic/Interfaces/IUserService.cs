﻿using Billennium.OLX.BusinessLogic.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Billennium.OLX.BusinessLogic.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDto>> GetAll();
        Task<RegisterDTO> Add(RegisterDTO UserDto);
        Task<UserUpdateDTO> Update(UserUpdateDTO userDto);
        Task<UserAuthenticationDto> Authenticate(UserAuthenticationDto userAuthDto);
        Task<UserDto>GetById(int id);
        Task Delete(int id);
    }
}
