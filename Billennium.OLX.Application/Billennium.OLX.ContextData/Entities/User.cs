﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Billennium.OLX.ContextData.Entities
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsPhoneNumberVisible { get; set; }
        public string Email { get; set; }
        public bool IsEmailVisible { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string HomeNumber { get; set; }
        public string PasswordHash { get; set; }
        public ICollection<Ad> Ads { get; set; }
        public ICollection<Favourites> Favourites { get; set; }

        public void AddPasswordHash(string hash)
        {
            PasswordHash = hash;
        }
    }
}
